# Развертывание пакета MLflow
![img.png](img.png)
juju deploy mlflow --channel=2.1/stable --trust

juju run-action mlflow-server/0  get-minio-credentials --wait

juju status --color | grep -E "blocked|error|maintenance|waiting|App|Unit" 

!printenv | grep MLFLOW
https://documentation.ubuntu.com/charmed-mlflow/en/latest/tutorial/mlflow-kubeflow/

![img_1.png](img_1.png)
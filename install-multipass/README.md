# Установка MicroK8s с помощью multipass

[Multipass](https://multipass.run) - это самый быстрый способ создать полноценную виртуальную машину Ubuntu под Linux, Windows или macOS, а также отличная база для использования MicroK8s.

Для операционных систем, поддерживающих привязку, ее можно установить одной командой:
```shell
sudo snap install multipass
```
> ⓘ **Примечание:** Для других платформ, пожалуйста, ознакомьтесь с последними руководствами по установке на [сайте Multipass](https://multipass.run/install)

Установив multipass, вы можете создать виртуальную машину для запуска MicroK8s. Для этого рекомендуется не менее 4 гигабайт оперативной памяти и 40 гигабайт дискового пространства - мы сможем выполнить эти требования при запуске ВМ:
```bash
multipass launch -n microk8s-vm -c2 -m4G -d20G
```
Теперь мы можем найти IP-адрес, который был выделен. Запускаем:
```bash
multipass list
```
... вернет что-то вроде:

```
Name                    State             IPv4             Release
microk8s-vm             RUNNING           192.168.64.2     Ubuntu 22.04 LTS
```
![img.png](img.png)
Запишите этот IP, так как службы будут доступны на нем при доступе с хост-машины.

Для более удобной работы в среде виртуальной машины можно запустить оболочку:
```bash
multipass shell microk8s-vm
```
Затем установите MicroK8s snap и настройте сеть:
```bash
sudo snap install microk8s --classic --channel=1.29/stable
sudo iptables -P FORWARD ACCEPT
```
Команда iptables необходима для разрешения трафика между виртуальной машиной и хостом.

Теперь, находясь в оболочке ВМ, вы можете следовать остальным инструкциям
[инструкции по быстрому запуску](/t/introduction-to-microk8s/11243)

#### Полезные команды multipass

- Получите оболочку внутри виртуальной машины:
  ```bash
  multipass shell microk8s-vm
  ```
- Выключите виртуальную машину:
  ```bash
  multipass stop microk8s-vm
  ```
- Удалите и очистите виртуальную машину:
  ```bash
  multipass delete microk8s-vm
  multipass purge
  ```




# MicroK8s Addons

There are two types of Addons, Core Addons maintained and officially supported by the MicroK8s team at Canonical, and Community Addons. 

See also:  [How to manage Addons][], [Setting up your own repository][]

### Addons in the Core Repository

| name | description | version | compatibility |
|------|----------|---------------|---------------|
| **[cert-manager][]** | Certificate management for Kubernetes clusters | ![1.25] | ![amd64] ![arm64] ![power] ![classic] ![strict] |
| **community** | Enables the community addons repository | ![1.24] | ![amd64]  ![arm64] ![power] ![s390] ![classic] ![strict] |
| **[dns][]** | Deploys CoreDNS. It is recommended that this addon is always enabled. | ![1.12] |  ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**[dashboard][]**| The standard Kubernetes Dashboard. | ![1.12] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**[gpu][]** | Enable support for GPU accelerated workloads using the NVIDIA runtime. | ![1.12] | ![amd64] ![classic] |
| **ha-cluster** | Allows for high availability on clusters with at least three nodes. | ![1.19] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**helm** | Installs the [Helm 3][] package manager for Kubernetes | ![1.15] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**helm3**| Transition addon introducing the [Helm 3][] package manager | ![1.18] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict]|
|**[hostpath-storage][]**|  Create a default storage class which allocates storage from a host directory. **Note!:** The  add-on uses simple filesystem storage local to the node where it was added. Not suitable for a production environment or clusters| ![1.12] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**[host-access][]**| Provides a fixed IP for access to the host's services. | ![1.19] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**[ingress][]**| A simple ingress controller for external access. | ![1.12] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**[kube-ovn][]**| The feature rich [Kube-OVN](https://www.kube-ovn.io/) network fabric. | ![1.25] | ![amd64] ![arm64] ![classic] |
|**[mayastor][]**| Multi-node zero-ops storage option powered by [Mayastor][mayastor-home] | ![1.24] | ![amd64] ![arm64] ![classic] |
| **[minio][]** | Cloud-agnostic S3-compatible object storage. | ![1.26] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict]|
|**[metallb][]**| Deploys the [MetalLB Loadbalancer][metallb-website]. Note that currently this does not work on macOS, due to  network filtering. | ![1.17] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**metrics-server**| Adds the [Kubernetes Metrics Server][metrics-design-doc] for API access to service metrics. | ![1.12] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**prometheus**| Deploys the [Prometheus Operator][prometheus-docs]. | ![1.14]| ![amd64] ![arm64] ![classic] ![strict] |
|**rbac**| Enable Role Based Access Control for authorisation. Note that this is incompatible with some other add-ons. |  ![1.14] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
| **[rook-ceph][]** | Rook turns distributed storage systems into self-managing, self-scaling, self-healing storage services. | ![1.28] | ![amd64] ![arm64] |
| **registry** | Deploy a private image registry and expose it on localhost:32000. | ![1.12] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
|**storage**| DEPRECATED  - Replaced by the **hostpath-storage** addon.  | ![1.12] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |

<h3 id="heading--community">Addons in the Community Repository</h3>

This collection of third party and community maintained addons can be enabled with:

```bash

microk8s enable community

```

Here is what is currently included:

| name | description | version | compatibility |
|------|----------|---------------|---------------|
|[ambassador][] | [Ambassador](https://www.getambassador.io/) is an API and Ingress controller | ![1.19] ![<1.24] | ![amd64] ![classic] ![strict] |
| [argocd][] | Deploys [Argo CD][argocd], the declarative, GitOps continuous delivery tool for Kubernetes.| ![1.24] | ![amd64] ![classic] |
|cilium | Deploys [Cilium][cilium-doc] to support Kubernetes network policies using eBPF. | ![1.15] | ![amd64] ![classic] ![strict] |
| [cloudnative-pg] | Adds cloud native Postgres support for faster and more reliable PG | ![1.29] | ![amd64][] ![arm64][] |
| [easyhaproxy][] | Adds EasyHAProxy for automatic ingress. | ![1.27]  | ![amd64][] ![arm64][]  ![strict][] ![classic][] |
| [falco][] |Cloud native security alerts | ![1.29] | ![amd64][] ![arm64][] |
|  [fluentd][] |  Deploy the [Elasticsearch-Fluentd-Kibana][kibana-docs] logging and monitoring solution. | ![1.13] | ![amd64] ![classic] |
| [gopaddle-lite][] | Simple "no-code" platform for Kubernetes developers.  |![1.26]| ![amd64] ![arm64] ![classic] |
|  [inaccel][] |  Simplify FPGA management and application lifecycle with InAccel. | ![1.24] | ![amd64] ![classic] ![strict] |
|  istio |  Adds the core [Istio][istio-docs] services (not available on arm64 arch). | ![1.12] | ![amd64] ![classic] ![strict] |
|  jaeger |  Deploy the [Jaeger Operator][jaeger-docs] in the “simplest” configuration. | ![1.13] | ![amd64] ![arm64]  ![classic] ![strict] |
|  [kata][] |  Adds [Kata containers](https://katacontainers.io/) support - A secure container runtime with lightweight virtual machines. | ![1.22] | ![amd64] ![classic] |
|  [keda][] |  Deploys [KEDA](https://keda.sh/) - Kubernetes Event-driven Autoscaling operator. | ![1.20] | ![amd64] ![classic] ![strict] |
|  knative |  Adds the [Knative][knative-docs] middleware to your cluster | ![1.15] | ![amd64] ![arm64] ![power] ![s390] ![classic] ![strict] |
| [kubearmor][] | Provides policy-based security for workloads. See [KubeArmor website](https://kubearmor.io/) . | ![1.28][] |![amd64][] ![arm64][] ![classic][]|
| [kwasm][] |  Add WebAssembly support to your Kubernetes nodes | ![1.26] | ![amd64] ![arm64] ![classic] ![strict] |
|  [linkerd][] |  Deploys the [linkerd][linkerd-docs] service mesh | ![1.19] | ![amd64] ![arm64] ![classic]| 
| [microcks][] |  Open source Kubernetes native tool for API Mocking and Testing. Microcks is a [Cloud Native Computing](https://www.cncf.io/) Sandbox project. | ![1.28] | ![amd64] ![arm64] ![classic] ![strict] |
|  [multus][] | Add multus for multiple network capability. | ![1.19] | ![amd64] ![arm64] ![classic] |
|  [nfs][] | Adds NFS-based storage. | ![1.25] | ![amd64] ![classic] |
| [ngrok][] | Unified ingress/gateway  | ![1.29] | ![amd64][] ![arm64][] ![classic] |
| [ondat][] | A Kubernetes-native persistent storage platform.  | ![1.26] | ![amd64] ![classic] |
|  [openebs][] |  Add [OpenEBS](https://openebs.io/) storage capability.  | ![1.21] | ![amd64] ![arm64]  ![classic] ![strict] |
|  [openfaas][] |  [OpenFaaS](https://www.openfaas.com/), the popular serverless framework. | ![1.21] | ![amd64] ![classic] ![strict] |
|  [osm-edge][] |   Open Service Mesh Edge (OSM-Edge) fork from [Open Service Mesh](https://github.com/openservicemesh/osm) is a lightweight, extensible, Cloud Native [service mesh](https://en.wikipedia.org/wiki/Service_mesh) built for Edge computing. | ![1.25] | ![amd64] ![arm64]  ![classic] ![strict] |
| [parking][] | Parking for static sites | ![1.27] | ![amd64][] ![arm64][]  ![strict][] ![classic][]
|  [portainer][] |  Container management dashboard (see [portainer.io](https://portainer.io)). | ![1.20] | ![amd64] ![arm64]  ![classic] ![strict] |
| [shifu][] | Kubernetes native IoT development framework | ![1.27] | ![amd64][] ![arm64][] |
| [sosivio][] | Predictive troubleshooting for Kubernetes | ![1.26] | ![amd64] ![classic] ![strict] |
|  [traefik][] | Adds the [Traefik Kubernetes Ingress controller](https://doc.traefik.io/traefik/providers/kubernetes-ingress/).| ![1.20] | ![amd64] ![arm64]  ![classic] ![strict] |
| [trivy][] | Open source security scanner for Kubernetes | ![1.26] | ![amd64][] ![arm64][] ![classic][] ![strict][] |



<!-- BADGES -->

[win]: https://img.shields.io/badge/Windows-%E2%9C%93-blue
[mac]: https://img.shields.io/badge/macOS-%E2%9C%93-lightgrey
[arm64]: https://img.shields.io/badge/arm64-%E2%9C%93-orange
[amd64]: https://img.shields.io/badge/amd64-%E2%9C%93-383
[power]: https://img.shields.io/badge/POWER-%E2%9C%93-699
[s390]: https://img.shields.io/badge/S390-%E2%9C%93-484
[strict]: https://img.shields.io/badge/strict-%E2%9C%93-85f
[classic]: https://img.shields.io/badge/classic-%E2%9C%93-338
[<1.22]: https://img.shields.io/badge/Prior%20to-1.22-red?logo=kubernetes&labelColor=ddd
[<1.23]: https://img.shields.io/badge/Prior%20to-1.23-red?logo=kubernetes&labelColor=ddd
[<1.24]: https://img.shields.io/badge/Prior%20to-1.24-red?logo=kubernetes&labelColor=ddd
[<1.25]: https://img.shields.io/badge/Prior%20to-1.25-red?logo=kubernetes&labelColor=ddd
[1.12]: https://img.shields.io/badge/From-1.12-2a2?logo=kubernetes&labelColor=ddd
[1.13]: https://img.shields.io/badge/From-1.13-2a2?logo=kubernetes&labelColor=ddd
[1.14]: https://img.shields.io/badge/From-1.14-2a2?logo=kubernetes&labelColor=ddd
[1.15]: https://img.shields.io/badge/From-1.15-2a2?logo=kubernetes&labelColor=ddd
[1.16]: https://img.shields.io/badge/From-1.16-2a2?logo=kubernetes&labelColor=ddd
[1.17]: https://img.shields.io/badge/From-1.17-2a2?logo=kubernetes&labelColor=ddd
[1.18]: https://img.shields.io/badge/From-1.18-2a2?logo=kubernetes&labelColor=ddd
[1.19]: https://img.shields.io/badge/From-1.19-2a2?logo=kubernetes&labelColor=ddd
[1.20]: https://img.shields.io/badge/From-1.20-2a2?logo=kubernetes&labelColor=ddd
[1.21]: https://img.shields.io/badge/From-1.21-2a2?logo=kubernetes&labelColor=ddd
[1.22]: https://img.shields.io/badge/From-1.22-2a2?logo=kubernetes&labelColor=ddd
[1.23]: https://img.shields.io/badge/From-1.23-2a2?logo=kubernetes&labelColor=ddd
[1.24]: https://img.shields.io/badge/From-1.24-2a2?logo=kubernetes&labelColor=ddd
[1.25]: https://img.shields.io/badge/From-1.25-2a2?logo=kubernetes&labelColor=ddd
[1.26]: https://img.shields.io/badge/From-1.26-2a2?logo=kubernetes&labelColor=ddd
[1.27]: https://img.shields.io/badge/From-1.27-2a2?logo=kubernetes&labelColor=ddd
[1.28]: https://img.shields.io/badge/From-1.28-2a2?logo=kubernetes&labelColor=ddd
[1.29]: https://img.shields.io/badge/From-1.29-2a2?logo=kubernetes&labelColor=ddd
[1.30]: https://img.shields.io/badge/From-1.30-2a2?logo=kubernetes&labelColor=ddd
[1.31]: https://img.shields.io/badge/From-1.31-2a2?logo=kubernetes&labelColor=ddd
<!-- LINKS -->

[ambassador]: /t/add-on-ambassador/11806
[argocd]: https://argo-cd.readthedocs.io/en/stable/
[dashboard]: /t/add-on-dashboard/11289
[dns]: /t/addon-dns/11287
[fluentd]: /t/addon-fluentd/11262
[gpu]: /t/addon-gpu/11286
[ingress]: /t/addon-ingress/11259
[linkerd]: /t/add-on-linkerd/11285
[host-access]: /t/add-on-host-access/11260
[multus]: /t/add-on-multus/11664
[charmed-kubeflow]: http://charmed-kubeflow.io/
[cilium-doc]: http://docs.cilium.io/en/stable/intro/
[cloudnative-pg]: /t/addon-cloudnative-pg/26228
[easyhaproxy]: /t/addon-easyhaproxy/23183
[efk-upstream]: https://kubernetes.io/docs/tasks/debug-application-cluster/logging-elasticsearch-kibana/
[falco]: /t/addon-falco/26227
[gopaddle-lite]: /t/addon-gopaddle-lite/22120
[Helm 2]: https://helm.sh
[Helm 3]: https://helm.sh
[inaccel]: /t/addon-inaccel/17522
[istio-woe]: https://istio.io/docs/concepts/what-is-istio/
[istio-docs]: https://istio.io/docs/
[jaeger-docs]: https://github.com/jaegertracing/jaeger-operator
[juju]: https://jaas.ai/docs/what-is-juju
[linkerd-docs]: https://linkerd.io/2/overview/
[kibana-docs]: https://www.elastic.co/guide/en/kibana/current/discover.html
[KubeFlow]: https://www.kubeflow.org/
[kubeflow-tutorial]: https://charmed-kubeflow.io/docs/quickstart
[kubeflow-addon]: /t/add-on-kubeflow/12089
[metrics-design-doc]: https://github.com/kubernetes-sigs/metrics-server
[mayastor]: /t/addon-openebs-mayastor-clustered-storage-v2-0-0/23493
[microcks]: /t/addon-microcks/24904
[minio]: /t/addon-minio/22132
[knative-docs]: https://knative.dev/
[prometheus-docs]: https://prometheus.io/docs/
[mayastor-home]: https://github.com/openebs/mayastor
[metallb-website]: https://metallb.universe.tf/
[metallb]: /t/add-on-metallb/11790
[nfs]: /t/addon-nfs-server-provisioner/20903
| [ondat][] | A Kubernetes-native persistent storage platform.  | ![1.26] | ![amd64] ![classic] |
|  [openebs][] |  Add [OpenEBS](https://openebs.io/) storage capability.  | ![1.21] | ![amd64] ![arm64]  ![classic] ![strict] |
|  [openfaas][] |  [OpenFaaS](https://www.openfaas.com/), the popular serverless framework. | ![1.21] | ![amd64] ![classic] ![strict] |
|  [osm-edge][] |   Open Service Mesh Edge (OSM-Edge) fork from [Open Service Mesh](https://github.com/openservicemesh/osm) is a lightweight, extensible, Cloud Native [service mesh](https://en.wikipedia.org/wiki/Service_mesh) built for Edge computing. | ![1.25] | ![amd64] ![arm64]  ![classic] ![strict] |
| [parking][] | Parking for static sites | ![1.27] | ![amd64][] ![arm64][]  ![strict][] ![classic][]
|  [portainer][] |  Container management dashboard (see [portainer.io](https://portainer.io)). | ![1.20] | ![amd64] ![arm64]  ![classic] ![strict] |
| [shifu][] | Kubernetes native IoT development framework | ![1.27] | ![amd64][] ![arm64][] |
| [sosivio][] | Predictive troubleshooting for Kubernetes | ![1.26] | ![amd64] ![classic] ![strict] |
|  [traefik][] | Adds the [Traefik Kubernetes Ingress controller](https://doc.traefik.io/traefik/providers/kubernetes-ingress/).| ![1.20] | ![amd64] ![arm64]  ![classic] ![strict] |
| [trivy][] | Open source security scanner for Kubernetes | ![1.26] | ![amd64][] ![arm64][] ![classic][] ![strict][] |



<!-- BADGES -->

[win]: https://img.shields.io/badge/Windows-%E2%9C%93-blue
[mac]: https://img.shields.io/badge/macOS-%E2%9C%93-lightgrey
[arm64]: https://img.shields.io/badge/arm64-%E2%9C%93-orange
[amd64]: https://img.shields.io/badge/amd64-%E2%9C%93-383
[power]: https://img.shields.io/badge/POWER-%E2%9C%93-699
[s390]: https://img.shields.io/badge/S390-%E2%9C%93-484
[strict]: https://img.shields.io/badge/strict-%E2%9C%93-85f
[classic]: https://img.shields.io/badge/classic-%E2%9C%93-338
[<1.22]: https://img.shields.io/badge/Prior%20to-1.22-red?logo=kubernetes&labelColor=ddd
[<1.23]: https://img.shields.io/badge/Prior%20to-1.23-red?logo=kubernetes&labelColor=ddd
[<1.24]: https://img.shields.io/badge/Prior%20to-1.24-red?logo=kubernetes&labelColor=ddd
[<1.25]: https://img.shields.io/badge/Prior%20to-1.25-red?logo=kubernetes&labelColor=ddd
[1.12]: https://img.shields.io/badge/From-1.12-2a2?logo=kubernetes&labelColor=ddd
[1.13]: https://img.shields.io/badge/From-1.13-2a2?logo=kubernetes&labelColor=ddd
[1.14]: https://img.shields.io/badge/From-1.14-2a2?logo=kubernetes&labelColor=ddd
[1.15]: https://img.shields.io/badge/From-1.15-2a2?logo=kubernetes&labelColor=ddd
[1.16]: https://img.shields.io/badge/From-1.16-2a2?logo=kubernetes&labelColor=ddd
[1.17]: https://img.shields.io/badge/From-1.17-2a2?logo=kubernetes&labelColor=ddd
[1.18]: https://img.shields.io/badge/From-1.18-2a2?logo=kubernetes&labelColor=ddd
[1.19]: https://img.shields.io/badge/From-1.19-2a2?logo=kubernetes&labelColor=ddd
[1.20]: https://img.shields.io/badge/From-1.20-2a2?logo=kubernetes&labelColor=ddd
[1.21]: https://img.shields.io/badge/From-1.21-2a2?logo=kubernetes&labelColor=ddd
[1.22]: https://img.shields.io/badge/From-1.22-2a2?logo=kubernetes&labelColor=ddd
[1.23]: https://img.shields.io/badge/From-1.23-2a2?logo=kubernetes&labelColor=ddd
[1.24]: https://img.shields.io/badge/From-1.24-2a2?logo=kubernetes&labelColor=ddd
[1.25]: https://img.shields.io/badge/From-1.25-2a2?logo=kubernetes&labelColor=ddd
[1.26]: https://img.shields.io/badge/From-1.26-2a2?logo=kubernetes&labelColor=ddd
[1.27]: https://img.shields.io/badge/From-1.27-2a2?logo=kubernetes&labelColor=ddd
[1.28]: https://img.shields.io/badge/From-1.28-2a2?logo=kubernetes&labelColor=ddd
[1.29]: https://img.shields.io/badge/From-1.29-2a2?logo=kubernetes&labelColor=ddd
[1.30]: https://img.shields.io/badge/From-1.30-2a2?logo=kubernetes&labelColor=ddd
[1.31]: https://img.shields.io/badge/From-1.31-2a2?logo=kubernetes&labelColor=ddd
<!-- LINKS -->

[ambassador]: /t/add-on-ambassador/11806
[argocd]: https://argo-cd.readthedocs.io/en/stable/
[dashboard]: /t/add-on-dashboard/11289
[dns]: /t/addon-dns/11287
[fluentd]: /t/addon-fluentd/11262
[gpu]: /t/addon-gpu/11286
[ingress]: /t/addon-ingress/11259
[linkerd]: /t/add-on-linkerd/11285
[host-access]: /t/add-on-host-access/11260
[multus]: /t/add-on-multus/11664
[charmed-kubeflow]: http://charmed-kubeflow.io/
[cilium-doc]: http://docs.cilium.io/en/stable/intro/
[cloudnative-pg]: /t/addon-cloudnative-pg/26228
[easyhaproxy]: /t/addon-easyhaproxy/23183
[efk-upstream]: https://kubernetes.io/docs/tasks/debug-application-cluster/logging-elasticsearch-kibana/
[falco]: /t/addon-falco/26227
[gopaddle-lite]: /t/addon-gopaddle-lite/22120
[Helm 2]: https://helm.sh
[Helm 3]: https://helm.sh
[inaccel]: /t/addon-inaccel/17522
[istio-woe]: https://istio.io/docs/concepts/what-is-istio/
[istio-docs]: https://istio.io/docs/
[jaeger-docs]: https://github.com/jaegertracing/jaeger-operator
[juju]: https://jaas.ai/docs/what-is-juju
[linkerd-docs]: https://linkerd.io/2/overview/
[kibana-docs]: https://www.elastic.co/guide/en/kibana/current/discover.html
[KubeFlow]: https://www.kubeflow.org/
[kubeflow-tutorial]: https://charmed-kubeflow.io/docs/quickstart
[kubeflow-addon]: /t/add-on-kubeflow/12089
[metrics-design-doc]: https://github.com/kubernetes-sigs/metrics-server
[mayastor]: /t/addon-openebs-mayastor-clustered-storage-v2-0-0/23493
[microcks]: /t/addon-microcks/24904
[minio]: /t/addon-minio/22132
[knative-docs]: https://knative.dev/
[prometheus-docs]: https://prometheus.io/docs/
[mayastor-home]: https://github.com/openebs/mayastor
[metallb-website]: https://metallb.universe.tf/
[metallb]: /t/add-on-metallb/11790
[nfs]: /t/addon-nfs-server-provisioner/20903
[ngrok]: /t/addon-ngrok/26411
[ondat]: /t/addon-ondat/22161
[portainer]: /t/addon-portainer/13714
[parking]: /t/addon-parking/23186
[rook-ceph]: /t/addon-rook-ceph/25042
[traefik]: /t/addon-traefik-ingress/13742
[kata]: /t/addon-kata/17010
[keda]: /t/addon-keda/14011
[kwasm]: /t/addon-kwasm/22054
[openebs]: /t/addon-openebs/15229
[openfaas]: /t/addon-openfaas/14981
[kube-ovn]: /t/addon-kubeovn/20700
[kubearmor]: /t/addon-kubearmor/24862
[hostpath-storage]: /t/addon-hostpath-storage/20878
[osm-edge]: /t/addon-osm-edge/20969
[sosivio]: /t/addon-sosivio/21458
[shifu]: /t/addon-shifu/23327
[trivy]: /t/addon-trivy/23797
[cert-manager]: /t/addon-cert-manager/21122
[How to manage Addons]: /t/how-to-manage-addons/23799
[Setting up your own repository]: /t/use-edit-or-create-addons/19796#setup-your-own-repository-3
[ondat]: /t/addon-ondat/22161
[portainer]: /t/addon-portainer/13714
[parking]: /t/addon-parking/23186
[rook-ceph]: /t/addon-rook-ceph/25042
[traefik]: /t/addon-traefik-ingress/13742
[kata]: /t/addon-kata/17010
[keda]: /t/addon-keda/14011
[kwasm]: /t/addon-kwasm/22054
[openebs]: /t/addon-openebs/15229
[openfaas]: /t/addon-openfaas/14981
[kube-ovn]: /t/addon-kubeovn/20700
[kubearmor]: /t/addon-kubearmor/24862
[hostpath-storage]: /t/addon-hostpath-storage/20878
[osm-edge]: /t/addon-osm-edge/20969
[sosivio]: /t/addon-sosivio/21458
[shifu]: /t/addon-shifu/23327
[trivy]: /t/addon-trivy/23797
[cert-manager]: /t/addon-cert-manager/21122
[How to manage Addons]: /t/how-to-manage-addons/23799
[Setting up your own repository]: /t/use-edit-or-create-addons/19796#setup-your-own-repository-3
​​
77  78 / 78
​​
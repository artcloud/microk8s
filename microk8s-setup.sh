#sudo apt install asciinema
#sudo snap install multipass
#multipass launch -n microk8s-vm -c2 -m4G -d20G
asciinema rec microk8s-vm.cast --overwrite
multipass shell microk8s-vm
sudo snap install microk8s --classic --channel=1.29
sudo iptables -P FORWARD ACCEPT
sudo usermod -a -G microk8s $USER
sudo mkdir -p ~/.kube
sudo chown -f -R $USER ~/.kube
exit
multipass shell microk8s-vm
microk8s status --wait-ready
echo "alias k='microk8s kubectl'" >>~/.bashrc
echo 'source <(microk8s kubectl completion bash)' >>~/.bashrc
echo 'complete -F __start_kubectl k' >>~/.bashrc
source ~/.bashrc
k create deployment nginx --image=nginx
k get po
microk8s stop
multipass stop microk8s-vm
multipass delete microk8s-vm
multipass purge
exit
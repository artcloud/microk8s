# Кластер MicroK8s
Хотя MicroK8s разработан как сверхлегкая реализация Kubernetes, создание кластера MicroK8s все же возможно и полезно. На этой странице рассказывается о том, как добавлять и удалять узлы и что требуется для обеспечения высокой доступности кластера.
> ⓘ **Примечание:** Каждому узлу кластера MicroK8s требуется собственное окружение для работы, будь то отдельная ВМ или контейнер на одной машине или другая машина в той же сети. Обратите внимание, что, как и почти для всех сетевых сервисов, для работы межузлового взаимодействия важно, чтобы в этих средах было правильное время (например, обновленное с сервера ntp).
## Добавление узла
Чтобы создать кластер из двух или более уже работающих экземпляров MicroK8s, используйте команду `microk8s add-node`. Экземпляр MicroK8s, на котором будет запущена эта команда, станет ведущим в кластере и будет размещать плоскость управления Kubernetes:
```bash
microk8s add-node
```
Это вернет некоторые инструкции по присоединению, которые должны быть выполнены **на экземпляре MicroK8s, который вы хотите присоединить к кластеру (НЕ на том узле, с которого вы выполнили команду `add-node`)**.
```
ubuntu@mk8s-1:~$ microk8s add-node
From the node you wish to join to this cluster, run the following:
microk8s join 185.204.117.3:25000/b83e00c714287a6fe61d9e74ac9c2d88/c59006c46eac

Use the '--worker' flag to join a node as a worker not running the control plane, eg:
microk8s join 185.204.117.3:25000/b83e00c714287a6fe61d9e74ac9c2d88/c59006c46eac --worker

If the node you are adding is not reachable through the default interface you can use one of the following:
microk8s join 185.204.117.3:25000/b83e00c714287a6fe61d9e74ac9c2d88/c59006c46eac
```
![img_3.png](img_3.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)
Присоединение узла к кластеру займет всего несколько секунд. После этого вы должны увидеть, что узел присоединился:
```bash
ubuntu@mk8s-1:~$ k get no
NAME     STATUS   ROLES    AGE     VERSION
mk8s-1   Ready    <none>   2d19h   v1.29.2
mk8s-2   Ready    <none>   3m6s    v1.29.2
mk8s-3   Ready    <none>   73s     v1.29.2
```
![img.png](img.png)
## Удаление узла
Сначала на узле, который вы хотите удалить, выполните команду `microk8s leave`. MicroK8s на удаляемом узле перезапустит свою собственную плоскость управления и возобновит работу как полноценный одноузловой кластер:
```bash
microk8s leave
```
![img_4.png](img_4.png)
![img_5.png](img_5.png)

Чтобы завершить удаление узла, вызовите `microk8s remove-node` с оставшихся узлов, чтобы указать, что ушедший (недоступный теперь) узел должен быть удален навсегда:
```bash
microk8s remove-node 185.204.117.7
microk8s remove-node 185.204.117.9
```
![img_7.png](img_7.png)
## Хранилище
Если вы используете простое хранилище, предоставляемое дополнением `hostpath storage`, обратите внимание, что оно будет доступно только для тех узлов, на которых оно было включено. Для кластерного хранилища необходимо настроить альтернативное хранилище. Например, см. руководство по использованию NFS.
## Высокая доступность
Начиная с версии 1.19 в MicroK8s HA включена по умолчанию. Если ваш кластер состоит из трех или более узлов, то хранилище данных будет реплицировано между узлами и будет устойчиво к единичному сбою (если на одном узле возникнут проблемы, рабочие нагрузки продолжат выполняться без перебоев).

В `microk8s status` теперь включена информация о состоянии HA. Например:
```
ubuntu@mk8s-1:~$ microk8s status
microk8s is running
high-availability: yes
  datastore master nodes: 185.204.117.3:19001 185.204.117.7:19001 185.204.117.9:19001
  datastore standby nodes: none
addons:
  enabled:
    dns                  # (core) CoreDNS
    ha-cluster           # (core) Configure high availability on the current node
    helm                 # (core) Helm - the package manager for Kubernetes
    helm3                # (core) Helm 3 - the package manager for Kubernetes
    hostpath-storage     # (core) Storage class; allocates storage from host directory
    storage              # (core) Alias to hostpath-storage add-on, deprecated
```
![img_6.png](img_6.png)
Дополнительную информацию о том, как работает HA, и как управлять кластером HA, можно найти на странице High Availability.
## Рабочие узлы
Начиная с версии 1.23 узел может присоединиться к кластеру в качестве рабочего узла. Рабочие узлы могут размещать рабочие нагрузки, но они не управляют плоскостью управления Kubernetes и поэтому не повышают доступность (HA) кластера. Рабочие узлы идеально подходят для низкопроизводительных устройств, поскольку они потребляют меньше ресурсов. Они также имеют смысл в больших кластерах с достаточным количеством узлов плоскости управления для обеспечения HA. Чтобы добавить рабочий узел, используйте флаг `--worker` при выполнении команды `microk8s join`:
```bash
microk8s join 185.204.117.3:25000/daaf7dee7d8d98322e1dfa1e01f7f2f2/c59006c46eac --worker
```
На рабочем узле запускается локальный прокси-сервер API, который обеспечивает связь между локальными службами (kubelet, kube-proxy) и серверами API, запущенными на нескольких узлах плоскости управления. При добавлении рабочего узла MicroK8s пытается обнаружить все конечные точки API-серверов в кластере и настроить новый узел соответствующим образом. Список серверов API хранится в файле `/var/snap/microk8s/current/args/traefik/provider.yaml`.

Прокси-сервер API будет автоматически проверять обновления при изменении узлов плоскости управления кластера (например, добавлении нового узла плоскости управления, удалении старого) и обновлять список известных конечных точек серверов API.

Если перед сервером API уже установлен балансировщик нагрузки, вы можете настроить адрес балансировщика нагрузки вручную в файле `/var/snap/microk8s/current/args/traefik/provider.yaml`. В этом случае не забудьте также отключить автоматическое обновление конечных точек плоскости управления, установив `--refresh-interval 0` в `/var/snap/microk8s/current/args/apiserver-proxy`.

# Что такое Charmed Kubeflow?
Charmed Kubeflow - это готовая к производству сквозная платформа [MLOps](https://ubuntu.com/blog/what-is-mlops) с открытым исходным кодом на базе нативных облачных технологий.

Charmed Kubeflow преобразует шаги Machine Learning в полноценные рабочие процессы, позволяя обучать, настраивать и отправлять модели Machine Learning (ML). Это позволяет автоматизировать рабочие процессы, повысить качество моделей и упростить развертывание рабочих нагрузок ML в производстве надежным способом.

Charmed Kubeflow удовлетворяет потребность в создании приложений ML структурированным и последовательным образом, способствуя повышению производительности и улучшению сотрудничества в командах Data Science.

Для Data Scientists и Machine Learning Engineers Charmed Kubeflow предоставляет расширенный набор инструментов для организации и масштабирования их работы.

### Как начать работу с Charmed Kubeflow?

Представьте, что вашей организации требуется полноценная платформа MLOps, на которой ученые, изучающие данные, инженеры ML и инженеры MLOps могли бы сотрудничать в продвижении передовых решений ML. Популярной платформой, отвечающей этим требованиям, является `Kubeflow`. Однако развертывание и поддержка Kubeflow производственного уровня - это огромная задача, требующая множества ручных настроек. А развертывание его в вашей собственной уникальной среде может привести к проблемам, с которыми никто раньше не сталкивался, - проблемам, которые вам придется решать самостоятельно.

Появился герой нашей истории: Charmed Kubeflow. Charmed Kubeflow - это набор шармов, который предоставляет вам простой, готовый к использованию способ развертывания Kubeflow с помощью одной команды. Он устанавливает разумные настройки по умолчанию, но при этом позволяет вам гибко настраивать конфигурацию по своему усмотрению - лучшее из двух миров. Он также наделяет ваше развертывание всеми суперспособностями `Juju`.

Итак, давайте испачкаем руки. В оставшейся части этой статьи мы собираемся развернуть Charmed Kubeflow для себя! Не волнуйтесь, используя Juju, мы выполним это развертывание с минимальными усилиями.

**Требования:**

В этом статье предполагается, что вы развернете Kubeflow на виртуальной машине публичного облака со следующими характеристиками:

* Работает под управлением Ubuntu 22.04 (jammy).
* Имеет как минимум 4 ядра, 32 ГБ оперативной памяти и 50 ГБ дискового пространства.
* Подключен к интернету для загрузки необходимых привязок и чармов.
* Установлен `python3`.

### Как настроить доступ к виртуальной машине по SSH с переадресацией портов?
Ниже показано, как подключить ноутбук к виртуальной машине с помощью SSH и проброса портов. 

Чтобы настроить туннель SSH с прокси-сервером SOCKS, выполните следующие действия:
1. Для Charmed Kubeflow выполните эту команду:
```shell
ssh -L 31380:localhost:31380 -D 9999 ubuntu@kubeflow-3.mk8s.by
```
![img_1.png](img_1.png)
>Примечание: для MLflow вы увидите дополнительную опцию `-L 31380:localhost:31380`. Это перенаправляет порт localhost `31380` на удаленную машину, поскольку он нужен нам для доступа к панели MLflow.
2. Настройте сетевые параметры браузера для использования прокси-сервера SOCKS
- Откройте `Настройки > Настройки сети > Настроить...`. 
- Установите для сетевого прокси значение `Ручная настройка прокси`. 
- Установите для хоста `SOCKS` значение: `127.0.0.1` и порт `9999`.

![img.png](img.png)

>Примечание: После этого весь сетевой трафик будет проходить через SSH-туннель к виртуальной машине. Если SSH-туннель будет закрыт, то вы потеряете интернет.

Чтобы отключить, сначала измените настройки сети, отключив сетевой прокси, а затем закройте SSH-туннель.

В остальной части этой статьи, если не указано иное, предполагается, что вы будете выполнять все операции командной строки на ВМ через открытый SSH-туннель. Также предполагается, что для доступа к панели Kubeflow вы будете использовать веб-браузер на своей локальной машине.

## Установка и подготовка MicroK8s.

Давайте начнем наше развертывание. Каждому экземпляру Kubeflow нужен кластер K8s для работы. Для простоты мы запустим наш экземпляр Kubeflow на [MicroK8s](https://microk8s.io/), который является самым простым и быстрым способом создания кластера K8s.

Установите пакет `microk8s` из канала `1.29/stable`.
```shell
sudo snap install microk8s --classic --channel=1.29
```
Отлично! Теперь `microk8s` установлен и автоматически начнет работать в фоновом режиме. Теперь пришло время настроить его, чтобы он был готов к работе с Kubeflow.

Далее, небольшой лайвхак. Чтобы не использовать `sudo` для каждой команды MicroK8s, выполните следующие команды:
```shell
sudo usermod -a -G microk8s $USER
newgrp microk8s
```
>Примечание: Вам нужно будет повторно запускать `newgrp microk8s` каждый раз, когда вы открываете новый сеанс оболочки.

Теперь, хотя мы будем использовать `juju` в качестве основного инструмента настройки для работы с развертыванием Kubeflow, иногда нам нужно будет взаимодействовать с MicroK8s напрямую через команду `kubectl`. Чтобы это работало, нам нужно предоставить право собственности на все конфигурационные файлы `kubectl` пользователю, выполняющему `kubectl`. Для этого выполните эту команду:
```shell
sudo chown -f -R $USER ~/.kube
```
Отлично! Это все, что касается установки прав доступа. Следующая остановка: дополнения!

MicroK8s - это полностью функциональный Kubernetes, работающий с минимальным количеством накладных расходов. Однако для наших целей нам понадобится Kubernetes с несколькими дополнительными функциями. Множество дополнительных сервисов доступны в виде [аддонов](https://microk8s.io/docs/addons) MicroK8s - кода, который поставляется вместе со снапом и может быть включен или выключен, когда это необходимо. Давайте включим некоторые из этих функций, чтобы получить Kubernetes, на котором мы сможем установить Kubeflow. Мы добавим службу DNS, чтобы приложения могли находить друг друга; хранилище; ингресс-контроллер, чтобы мы могли получить доступ к компонентам Kubeflow; и приложение-балансировщик нагрузки MetalLB.

Включите следующие дополнения Microk8s, чтобы настроить кластер Kubernetes с дополнительными сервисами, необходимыми для запуска Charmed Kubeflow: `dns`, `hostpath-storage`, `ingress` и `metallb` для диапазона IP-адресов `10.64.140.43-10.64.140.49`.

Включить аддоны MicroK8s
```shell
microk8s enable dns hostpath-storage ingress metallb:10.64.140.43-10.64.140.49 rbac
```
![img_2.png](img_2.png)
![img_3.png](img_3.png)
Отличная работа! Теперь вы установили и настроили MicroK8s.

Теперь может пройти 5 минут или около того, прежде чем все аддоны, которые мы настроили, будут готовы к работе.

Выполните следующую команду:
```shell
microk8s status --wait-ready
```
В выводе MicroK8s должен отображаться как запущенный, а все аддоны, которые мы включили ранее, должны быть перечислены как `enabled:`: `dns`, `storage`, `ingress` и `metallb`. Если это не так, подождите еще немного и выполните команду снова.
![img_4.png](img_4.png)
Отлично, теперь вы установили и настроили MicroK8s, и он запущен и готов! Теперь у нас есть кластер K8s, на котором мы можем развернуть наш экземпляр Kubeflow.
## Установка Juju
Juju - это менеджер жизненного цикла операций (OLM) для облаков, голого железа или Kubernetes. Мы будем использовать его для развертывания и управления компонентами, составляющими Kubeflow.

Чтобы установить Juju из snap, выполните следующую команду:
```shell
sudo snap install juju --channel=3.4
```
![img_5.png](img_5.png)
На некоторых машинах может отсутствовать папка, необходимая для корректной работы juju. В связи с этим, пожалуйста, убедитесь, что вы создали эту папку:
```shell
mkdir -p ~/.local/share
```
В качестве следующего шага мы можем настроить microk8s для корректной работы с juju, выполнив следующие действия:
```shell
microk8s config | juju add-k8s my-k8s --client
```
Команда `microk8s config` извлекает конфигурацию kubernetes клиента, которая затем регистрируется в конечных точках juju kubernetes.
![img_6.png](img_6.png)
Теперь выполните следующую команду, чтобы развернуть контроллер Juju на Kubernetes, который мы настроили с помощью MicroK8s:
```shell
juju bootstrap my-k8s uk8sx
```
Подождите, пока команда выполнится! Развертывание контроллера может занять минуту или две.

Контроллер - это агент Juju, работающий на Kubernetes, который можно использовать для развертывания и управления компонентами Kubeflow.
![img_7.png](img_7.png)
Далее нам нужно добавить модель для Kubeflow в контроллер. Выполните следующую команду, чтобы добавить модель под названием `kubeflow`:
```shell
juju add-model kubeflow
```
Контроллер может работать с различными `моделями`, которые соотносятся 1:1 с пространствами имен в Kubernetes. В данном случае имя модели должно быть `kubeflow`, из-за предположения, сделанного в коде Kubeflow Dashboard.

Отличная работа: Juju теперь установлен и настроен для Kubeflow!
![img_8.png](img_8.png)
## Развертывание Charmed Kubeflow

Перед развертыванием выполните эти команды:
```shell
sudo sysctl fs.inotify.max_user_instances=2560
sudo sysctl fs.inotify.max_user_watches=1048576
```
Нам нужно выполнить вышеуказанные команды, потому что под капотом microk8s использует inotify для взаимодействия с файловой системой, а в kubeflow иногда превышаются стандартные лимиты inotify. Обновите `/etc/sysctl.conf` следующими строками, если вы хотите, чтобы эти команды сохранялись при перезагрузке машины:
```
fs.inotify.max_user_instances=2560
fs.inotify.max_user_watches=655360
```
![img_9.png](img_9.png)
Наконец, мы готовы развернуть Charmed Kubeflow! Выполните этот код, чтобы развернуть пакет Charmed Kubeflow с помощью Juju:
```shell
juju deploy kubeflow --trust --channel=1.8
```
Будьте терпеливы. Процесс развертывания может занять 5-10 минут.

А пока что, что здесь происходит? Взгляните на свой вывод.
<details>
<summary>Образец вывода</summary>
```ubuntu@kubeflow-3:~$ juju deploy kubeflow --trust --channel=1.8
Located bundle "kubeflow" in charm-hub, revision 414
Located charm "admission-webhook" in charm-hub, channel 1.8/stable
Located charm "argo-controller" in charm-hub, channel 3.3.10/stable
Located charm "dex-auth" in charm-hub, channel 2.36/stable
Located charm "envoy" in charm-hub, channel 2.0/stable
Located charm "istio-gateway" in charm-hub, channel 1.17/stable
Located charm "istio-pilot" in charm-hub, channel 1.17/stable
Located charm "jupyter-controller" in charm-hub, channel 1.8/stable
Located charm "jupyter-ui" in charm-hub, channel 1.8/stable
Located charm "katib-controller" in charm-hub, channel 0.16/stable
Located charm "mysql-k8s" in charm-hub, channel 8.0/stable
Located charm "katib-db-manager" in charm-hub, channel 0.16/stable
Located charm "katib-ui" in charm-hub, channel 0.16/stable
Located charm "kfp-api" in charm-hub, channel 2.0/stable
Located charm "mysql-k8s" in charm-hub, channel 8.0/stable
Located charm "kfp-metadata-writer" in charm-hub, channel 2.0/stable
Located charm "kfp-persistence" in charm-hub, channel 2.0/stable
Located charm "kfp-profile-controller" in charm-hub, channel 2.0/stable
Located charm "kfp-schedwf" in charm-hub, channel 2.0/stable
Located charm "kfp-ui" in charm-hub, channel 2.0/stable
Located charm "kfp-viewer" in charm-hub, channel 2.0/stable
Located charm "kfp-viz" in charm-hub, channel 2.0/stable
Located charm "knative-eventing" in charm-hub, channel 1.10/stable
Located charm "knative-operator" in charm-hub, channel 1.10/stable
Located charm "knative-serving" in charm-hub, channel 1.10/stable
Located charm "kserve-controller" in charm-hub, channel 0.11/stable
Located charm "kubeflow-dashboard" in charm-hub, channel 1.8/stable
Located charm "kubeflow-profiles" in charm-hub, channel 1.8/stable
Located charm "kubeflow-roles" in charm-hub, channel 1.8/stable
Located charm "kubeflow-volumes" in charm-hub, channel 1.8/stable
Located charm "metacontroller-operator" in charm-hub, channel 3.0/stable
Located charm "minio" in charm-hub, channel ckf-1.8/stable
Located charm "mlmd" in charm-hub, channel 1.14/stable
Located charm "oidc-gatekeeper" in charm-hub, channel ckf-1.8/stable
Located charm "pvcviewer-operator" in charm-hub, channel 1.8/stable
Located charm "seldon-core" in charm-hub, channel 1.17/stable
Located charm "tensorboard-controller" in charm-hub, channel 1.8/stable
Located charm "tensorboards-web-app" in charm-hub, channel 1.8/stable
Located charm "training-operator" in charm-hub, channel 1.7/stable
Executing changes:
- upload charm admission-webhook from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application admission-webhook from charm-hub with 1 unit with 1.8/stable
  added resource oci-image
- upload charm argo-controller from charm-hub from channel 3.3.10/stable with architecture=amd64
- deploy application argo-controller from charm-hub with 1 unit with 3.3.10/stable
  added resource oci-image
- upload charm dex-auth from charm-hub from channel 2.36/stable with architecture=amd64
- deploy application dex-auth from charm-hub with 1 unit with 2.36/stable
  added resource oci-image
- upload charm envoy from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application envoy from charm-hub with 1 unit with 2.0/stable
WARNING deploying podspec charm "envoy": podspec charms are deprecated. Support for them will be removed soon.
  added resource oci-image
- upload charm istio-gateway from charm-hub from channel 1.17/stable with architecture=amd64
- deploy application istio-ingressgateway from charm-hub with 1 unit with 1.17/stable using istio-gateway
- upload charm istio-pilot from charm-hub from channel 1.17/stable with architecture=amd64
- deploy application istio-pilot from charm-hub with 1 unit with 1.17/stable
- upload charm jupyter-controller from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application jupyter-controller from charm-hub with 1 unit with 1.8/stable
  added resource oci-image
- upload charm jupyter-ui from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application jupyter-ui from charm-hub with 1 unit with 1.8/stable
  added resource oci-image
- upload charm katib-controller from charm-hub from channel 0.16/stable with architecture=amd64
- deploy application katib-controller from charm-hub with 1 unit with 0.16/stable
WARNING deploying podspec charm "katib-controller": podspec charms are deprecated. Support for them will be removed soon.
  added resource oci-image
- upload charm mysql-k8s from charm-hub from channel 8.0/stable with architecture=amd64
- deploy application katib-db from charm-hub with 1 unit with 8.0/stable using mysql-k8s
  added resource mysql-image
- upload charm katib-db-manager from charm-hub from channel 0.16/stable with architecture=amd64
- deploy application katib-db-manager from charm-hub with 1 unit with 0.16/stable
  added resource oci-image
- upload charm katib-ui from charm-hub from channel 0.16/stable with architecture=amd64
- deploy application katib-ui from charm-hub with 1 unit with 0.16/stable
  added resource oci-image
- upload charm kfp-api from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application kfp-api from charm-hub with 1 unit with 2.0/stable
  added resource oci-image
- deploy application kfp-db from charm-hub with 1 unit with 8.0/stable using mysql-k8s
  added resource mysql-image
- upload charm kfp-metadata-writer from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application kfp-metadata-writer from charm-hub with 1 unit with 2.0/stable
  added resource oci-image
- upload charm kfp-persistence from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application kfp-persistence from charm-hub with 1 unit with 2.0/stable
  added resource oci-image
- upload charm kfp-profile-controller from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application kfp-profile-controller from charm-hub with 1 unit with 2.0/stable
  added resource oci-image
- upload charm kfp-schedwf from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application kfp-schedwf from charm-hub with 1 unit with 2.0/stable
  added resource oci-image
- upload charm kfp-ui from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application kfp-ui from charm-hub with 1 unit with 2.0/stable
  added resource ml-pipeline-ui
- upload charm kfp-viewer from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application kfp-viewer from charm-hub with 1 unit with 2.0/stable
  added resource kfp-viewer-image
- upload charm kfp-viz from charm-hub from channel 2.0/stable with architecture=amd64
- deploy application kfp-viz from charm-hub with 1 unit with 2.0/stable
  added resource oci-image
- upload charm knative-eventing from charm-hub from channel 1.10/stable with architecture=amd64
- deploy application knative-eventing from charm-hub with 1 unit with 1.10/stable
- upload charm knative-operator from charm-hub from channel 1.10/stable with architecture=amd64
- deploy application knative-operator from charm-hub with 1 unit with 1.10/stable
  added resource knative-operator-image
  added resource knative-operator-webhook-image
- upload charm knative-serving from charm-hub from channel 1.10/stable with architecture=amd64
- deploy application knative-serving from charm-hub with 1 unit with 1.10/stable
- upload charm kserve-controller from charm-hub from channel 0.11/stable with architecture=amd64
- deploy application kserve-controller from charm-hub with 1 unit with 0.11/stable
  added resource kserve-controller-image
  added resource kube-rbac-proxy-image
- upload charm kubeflow-dashboard from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application kubeflow-dashboard from charm-hub with 1 unit with 1.8/stable
  added resource oci-image
- upload charm kubeflow-profiles from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application kubeflow-profiles from charm-hub with 1 unit with 1.8/stable
  added resource kfam-image
  added resource profile-image
- upload charm kubeflow-roles from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application kubeflow-roles from charm-hub with 1 unit with 1.8/stable
- upload charm kubeflow-volumes from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application kubeflow-volumes from charm-hub with 1 unit with 1.8/stable
WARNING deploying podspec charm "kubeflow-volumes": podspec charms are deprecated. Support for them will be removed soon.
  added resource oci-image
- upload charm metacontroller-operator from charm-hub from channel 3.0/stable with architecture=amd64
- deploy application metacontroller-operator from charm-hub with 1 unit with 3.0/stable
- upload charm minio from charm-hub from channel ckf-1.8/stable with architecture=amd64
- deploy application minio from charm-hub with 1 unit with ckf-1.8/stable
WARNING deploying podspec charm "minio": podspec charms are deprecated. Support for them will be removed soon.
  added resource oci-image
- upload charm mlmd from charm-hub from channel 1.14/stable with architecture=amd64
- deploy application mlmd from charm-hub with 1 unit with 1.14/stable
WARNING deploying podspec charm "mlmd": podspec charms are deprecated. Support for them will be removed soon.
  added resource oci-image
- upload charm oidc-gatekeeper from charm-hub from channel ckf-1.8/stable with architecture=amd64
- deploy application oidc-gatekeeper from charm-hub with 1 unit with ckf-1.8/stable
  added resource oci-image
- upload charm pvcviewer-operator from charm-hub for base ubuntu@20.04/stable from channel 1.8/stable with architecture=amd64
- deploy application pvcviewer-operator from charm-hub with 1 unit on ubuntu@20.04/stable with 1.8/stable
  added resource oci-image
  added resource oci-image-proxy
- upload charm seldon-core from charm-hub from channel 1.17/stable with architecture=amd64
- deploy application seldon-controller-manager from charm-hub with 1 unit with 1.17/stable using seldon-core
  added resource oci-image
- upload charm tensorboard-controller from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application tensorboard-controller from charm-hub with 1 unit with 1.8/stable
  added resource tensorboard-controller-image
- upload charm tensorboards-web-app from charm-hub from channel 1.8/stable with architecture=amd64
- deploy application tensorboards-web-app from charm-hub with 1 unit with 1.8/stable
  added resource tensorboards-web-app-image
- upload charm training-operator from charm-hub from channel 1.7/stable with architecture=amd64
- deploy application training-operator from charm-hub with 1 unit with 1.7/stable
  added resource training-operator-image
- add relation argo-controller - minio
- add relation dex-auth:oidc-client - oidc-gatekeeper:oidc-client
- add relation istio-pilot:ingress - dex-auth:ingress
- add relation istio-pilot:ingress - envoy:ingress
- add relation istio-pilot:ingress - jupyter-ui:ingress
- add relation istio-pilot:ingress - katib-ui:ingress
- add relation istio-pilot:ingress - kfp-ui:ingress
- add relation istio-pilot:ingress - kubeflow-dashboard:ingress
- add relation istio-pilot:ingress - kubeflow-volumes:ingress
- add relation istio-pilot:ingress - oidc-gatekeeper:ingress
- add relation istio-pilot:ingress-auth - oidc-gatekeeper:ingress-auth
- add relation istio-pilot:istio-pilot - istio-ingressgateway:istio-pilot
- add relation istio-pilot:ingress - tensorboards-web-app:ingress
- add relation istio-pilot:gateway-info - tensorboard-controller:gateway-info
- add relation katib-db-manager:relational-db - katib-db:database
- add relation kfp-api:relational-db - kfp-db:database
- add relation kfp-api:kfp-api - kfp-persistence:kfp-api
- add relation kfp-api:kfp-api - kfp-ui:kfp-api
- add relation kfp-api:kfp-viz - kfp-viz:kfp-viz
- add relation kfp-api:object-storage - minio:object-storage
- add relation kfp-profile-controller:object-storage - minio:object-storage
- add relation kfp-ui:object-storage - minio:object-storage
- add relation kserve-controller:ingress-gateway - istio-pilot:gateway-info
- add relation kserve-controller:local-gateway - knative-serving:local-gateway
- add relation kubeflow-profiles - kubeflow-dashboard
- add relation kubeflow-dashboard:links - jupyter-ui:dashboard-links
- add relation kubeflow-dashboard:links - katib-ui:dashboard-links
- add relation kubeflow-dashboard:links - kfp-ui:dashboard-links
- add relation kubeflow-dashboard:links - kubeflow-volumes:dashboard-links
- add relation kubeflow-dashboard:links - tensorboards-web-app:dashboard-links
- add relation mlmd:grpc - envoy:grpc
- add relation mlmd:grpc - kfp-metadata-writer:grpc
Deploy of bundle completed.
```
</details>

Здесь вы можете увидеть возможности использования `Juju` для развертывания `kubeflow`. Из вывода видно, что пакет находит, развертывает и настраивает все необходимые чары за нас. Без `juju` нам пришлось бы разворачивать и настраивать все эти чары самостоятельно.
![img_10.png](img_10.png)
Когда команда развертывания завершится, вы получите сообщение следующего содержания:
```
Deploy of bundle completed.
```
![img_11.png](img_11.png)
Это означает, что все компоненты пакета были запущены в работу. Однако это еще не означает, что `Kubeflow` готов. После развертывания различным компонентам пакета требуется некоторое время для инициализации и установления связи друг с другом. Наберитесь терпения - обычно это занимает от 15 минут до 1 часа.

Как же узнать, когда весь пакет будет готов? Это можно сделать с помощью команды `juju status`. Для начала давайте выполним основную команду status и посмотрим, что получится. Выполните следующую команду, чтобы вывести статус всех компонентов Juju:
```
juju status
```
![img_12.png](img_12.png)

Просмотрите полученные результаты. Вы должны увидеть краткую информацию, список приложений и связанную с ними информацию, а также список подразделений и связанную с ними информацию. Пока что не стоит слишком беспокоиться о том, что все это значит.
Главное, что нас интересует на этом этапе, - это статусы всех приложений и модулей в нашем пакете. Мы хотим, чтобы все статусы в конечном итоге стали `active`, указывая на то, что пакет готов. Выполните следующую команду, чтобы следить за компонентами, которые еще не активны:
```
juju status --watch 5s
```
При этом периодически будет выполняться команда `juju status`. Когда все компоненты на экране будут в состоянии `active`, мы будем знать, что наш бандл готов.

Не удивляйтесь, если время от времени статусы некоторых компонентов будут меняться на `blocked` или `error`. Это ожидаемое поведение, и эти статусы должны разрешиться сами собой по мере настройки пакета. Однако если компоненты по-прежнему находятся в одном и том же состоянии ошибки, обратитесь к шагам по устранению неполадок, приведенным ниже.
![img_13.png](img_13.png)
Пока вы ждете, пока пакет `Kubeflow` подготовится, переходите к следующему разделу этого статьи, в котором будут рассмотрены некоторые задачи по настройке после установки.

## Настройте доступ к приборной панели.

Запустить Kubeflow - это хорошо, но как мы будем взаимодействовать с ним как пользователь? Вот тут-то и приходит на помощь приборная панель. Мы перейдем к этому позже, а сейчас давайте настроим некоторые компоненты, чтобы получить доступ к приборной панели.

Во-первых, выполните эту команду, чтобы проверить IP-адрес балансировщика нагрузки `Istio ingress gateway`, который является точкой входа для всей нашей связки:
```shell
microk8s kubectl -n kubeflow get svc istio-ingressgateway-workload -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
```
![img_14.png](img_14.png)

Вы должны увидеть на выходе `10.64.140.43` - это IP-адрес данного компонента в конфигурации microk8s по умолчанию. Если вы видите что-то другое, не волнуйтесь - просто замените `10.64.140.43` на любой IP-адрес, который вы видите в оставшейся части инструкций этого руководства.

Чтобы получить доступ к `kubeflow` через его службу `dashboard`, нам нужно немного настроить пакет, чтобы он поддерживал аутентификацию и авторизацию. Для этого выполните следующие команды:
```shell
juju config dex-auth public-url=http://10.64.140.43.nip.io
juju config oidc-gatekeeper public-url=http://10.64.140.43.nip.io
```
![img_15.png](img_15.png)
Это сообщает компонентам аутентификации и авторизации пакета, что пользователи, обращающиеся к пакету, будут делать это через URL `http://10.64.140.43.nip.io`. В свою очередь, это позволяет этим компонентам создавать соответствующие ответы на входящий трафик.

Чтобы включить простую аутентификацию и задать имя пользователя и пароль для вашего развертывания Kubeflow, выполните следующие команды:
```shell
juju config dex-auth static-username=admin
juju config dex-auth static-password=admin
```
![img_16.png](img_16.png)
Не стесняйтесь использовать другой (более надежный!) пароль, если хотите.

Отлично! Теперь наш бандл настроен на доступ к приборной панели через браузер.

## Проверка развертывания Charmed Kubeflow
Отлично! Мы развернули и настроили Kubeflow. Но как узнать, что мы все сделали правильно? Можно попробовать войти в систему. Поехали!

Откройте браузер и перейдите по следующему URL-адресу:
```
http://10.64.140.43.nip.io
```
После этого вы увидите экран входа в систему dex. Введите имя пользователя (в нем указан адрес электронной почты, но подойдет любая строка, которую вы ввели при настройке) и пароль из предыдущего шага настройки.
![img_17.png](img_17.png)
Теперь вы должны увидеть страницу "Добро пожаловать" Kubeflow:
![img_18.png](img_18.png)
Нажмите кнопку `Start setup`. На следующем экране вам будет предложено создать пространство имен. Это просто способ сохранить все файлы и настройки одного проекта в одном, легкодоступном месте. Выберите любое имя, которое вам нравится:
![img_19.png](img_19.png)
После того как вы нажмете на кнопку `Finish`, на экране появится панель управления!
![img_20.png](img_20.png)
Поздравляю, мы развернули Kubeflow!

Более подробную информацию о работе с `kubeflow` можно найти в следующих статьях.

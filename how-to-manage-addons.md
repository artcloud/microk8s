# How to manage Addons
See also: [available Addons][]

To be as lightweight as possible, MicroK8s only installs the basics of a usable
Kubernetes install:

- api-server
- controller-manager
- scheduler
- kubelet
- cni
- kube-proxy

While this does deliver a pure Kubernetes experience with the smallest resource footprint possible, there are situations where you may require additional services. MicroK8s caters for this with the concept of "Addons" - extra services which can easily be added to MicroK8s. These addons can be enabled and disabled at any time, and most are pre-configured to 'just work' without any further set up.

For example, to enable the CoreDNS addon:

```bash
microk8s enable dns
```

These add-ons can be disabled at anytime using the `disable` command:

```bash
microk8s disable dns
```

... and you can check the list of available and installed addons at any time by running:

```bash
microk8s status
```

> ⓘ **Note:** Before installing add-ons which install extra client tools (e.g. Helm), be sure that the current user is part of the `microk8s` group, to avoid potential permission problems.

<h3 id="heading--repos">Addon Repositories</h3>

Starting from the v1.24 release, addons are organized in repositories. Repositories are essentially git projects in which users can place the addons they are interested in. To assist in creating a such repositories, the MicroK8s team maintains a [template repository](https://github.com/canonical/microk8s-addons-repo-template) on github that can be forked to start a new addons collection. Although the template repository includes a [HACKING.md guide](https://github.com/canonical/microk8s-addons-repo-template/blob/main/HACKING.md) with instructions on building a Python based demo addon, addons can be implemented in any programming language, even bash.

Addon repositories are found under `/var/snap/microk8s/common/addons` and can be edited in place. To ease the process of adding, removing and updating such repositories the `microk8s addons repo` command can be used. A repository under `https://github.com/my-org/awesome-repo` can be added on a MicroK8s cluster with:

```bash
microk8s addons repo add myrepo https://github.com/my-org/awesome-repo
```

An optional `--reference` flag can be used to point to a specific branch or tag inside the git repository.

The `myrepo` in the above example is the repository identifier. This identifier is used to differentiate addons with the same name found on separate addon repositories.

Removing and updating a repository is achieved with the `microk8s addons repo remove` and `microk8s addons repo update` commands respectively.

By default the MicroK8s ships with two repositories:

* [core](https://github.com/canonical/microk8s-core-addons/), with essential addons maintained by the MicroK8s team, and

* [community](https://github.com/canonical/microk8s-addons/), with community maintained addons.
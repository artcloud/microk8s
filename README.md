# MicroK8s

![](logo.png)

**MicroK8s** - это Kubernetes с низким уровнем производительности.

MicroK8s - это система с открытым исходным кодом для автоматизации развертывания, масштабирования и управления контейнерными приложениями. Она предоставляет функциональность основных компонентов Kubernetes, занимая небольшую площадь и масштабируясь от одного узла до производственного кластера высокой доступности.

Сокращая объем ресурсов, необходимых для работы Kubernetes, MicroK8s позволяет внедрять Kubernetes в новые среды, например:
* превращение Kubernetes в легкий инструмент разработки
* сделать Kubernetes доступным для использования в минимальных средах, таких как GitHub CI
* адаптация Kubernetes для IoT-приложений на небольших устройствах.

Разработчики используют MicroK8s в качестве недорогого полигона для отработки новых идей. В производстве ISV выигрывают за счет снижения накладных расходов и потребности в ресурсах, а также сокращения циклов разработки, что позволяет им выпускать устройства быстрее, чем когда-либо.

Экосистема MicroK8s включает десятки полезных аддонов - расширений, предоставляющих дополнительную функциональность и возможности. См. [список аддонов](addons.md), [как управлять аддонами](how-to-manage-addons.md).

## In this documentation

| | |
|--|--|
|  [Tutorials](/t/microk8s-tutorials/19416)</br>  Get started - a hands-on introduction to MicroK8s for new users </br> |  [How-to guides](/t/microk8s-how-to-guides/19442) </br> Step-by-step guides covering key operations and common tasks |
|  [Explanation](/t/microk8s-explanation/19446) </br> Concepts - discussion and clarification of key topics  | [Reference](/t/microk8s-reference/19447) </br> Technical information - specifications, commands, architecture |

---

## Project and community

MicroK8s is an open source project that welcomes community contributions, suggestions, fixes and constructive feedback. 

* [Our Code of Conduct](https://ubuntu.com/community/code-of-conduct)
* [Join the Discourse forum](https://discuss.kubernetes.io/)
* [Contribute to documentation](/t/13681)
* [Give us your feedback](/t/11261)
* [Join the MicroK8s community](/t/microk8s-community/25687)


## Navigation

<details>

| Level | Path | Navlink |
| -- | -- | -- |
| 0 | / | [Home](/t/11243) |
| 1 | tutorials | [Tutorials](/t/microk8s-tutorials/19416) |
| 2 | /getting-started | [Getting started](/t/introduction-to-microk8s/18344) |
| 1 | how-to | [How to Guides](/t/microk8s-how-to-guides/19442) | 
| 2 | install-alternatives| [Installing](/t/alternative-installs-macos-windows-10-multipass/11257) |
| 3 | install-windows | [...on Windows 10/11](/t/installing-microk8s-on-windows-10-11/21540) |
| 3 | install-macos | [...on macOS](/t/installing-microk8s-on-macos/21541) |
| 3 | install-raspberry-pi | [...on a Raspberry Pi ](/t/installing-microk8s-on-a-raspberry-pi/21542) |
| 3 | nvidia-dgx | [...on NVIDIA DGX](/t/microk8s-on-nvidia-dgx/18225) |
| 3 | install-lxd | [...with LXD ](/t/microk8s-in-lxd/11520) |
| 3 | install-multipass | [...using Multipass](/t/installing-microk8s-with-multipass/21544) |
| 3 | install-wsl2 | [... with WSL2](https://discuss.kubernetes.io/t/install-microk8s-on-wsl2/22877) |
| 3 | install-proxy | [...behind a proxy ](/t/installing-behind-a-proxy/11275) |
| 3 | install-offline | [...offline or airgapped](/t/installing-microk8s-offline-or-in-an-airgapped-environment/21545) |
| 3 | install-strict | [...with strict confinement](/t/strict-microk8s/21487) |
| 3 | install-alternatives | [Additional install notes](/t/alternative-installs-macos-windows-10-multipass/11257) |
| 3 | setting-snap-channel | [set channels/versions](/t/selecting-a-snap-channel/11270) |
| 2 | addons | [Addons](/t/microk8s-add-ons/11264) |
| 3 | how-to-manage-addons | [How to manage Addons](/t/how-to-manage-addons/23799) |
| 3 | howto-addons | [Create, edit and use community addons](https://discuss.kubernetes.io/t/use-edit-or-create-addons/19796) |
| 2 | config | Configuration |
| 3 | configuring-services | [Configure services](/t/configuring-microk8s-services/11269) |
| 3 | configure-host-interfaces | [Configure host interfaces](/t/configure-host-interfaces-used-by-microk8s/21508) |
| 3 | configure-cni | [Configure CNI](/t/microk8s-cni-configuration/23783) |
| 3 | add-launch-config | [Add launch configurations](/t/how-to-use-launch-configurations/23359) |
| 3 | how-to-dual-stack | [Configure Dual-stack (IPv4I&sol;Pv6)](/t/how-to-configure-dual-stack/24784) |
| 3 | change-cidr | [Change the pod CIDR](/t/microk8s-cni-configuration/23783) |
| 3 | external-lma | [Add an LMA stack](/t/microk8s-with-an-external-lma/13595) |
| 3 | external-etcd | [Use an external etcd cluster](https://discuss.kubernetes.io/t/using-an-external-etcd-with-microk8s/18351) |
| 3 | multi-user | [Set up MicroK8s for more users](/t/multi-user-microk8s/13275) |
| 3 | oidc-dex | [Set up OIDC to work with Dex](/t/configure-oidc-with-dex-for-a-microk8s-cluster/18339)
| 2 | cluster | Clusters |
| 3 | clustering | [Create a multi-node cluster](/t/clustering-with-microk8s/11276) |
| 3 | upgrade-cluster | [Upgrade a cluster](/t/upgrading-a-microk8s-cluster/18239) |
| 3 | high-availability | [Get High Availability](/t/high-availability-ha/11731) |
| 3 | restore-quorum | [Recover from a lost quorum on dqlite](/t/recover-from-lost-quorum/19160) |
| 2 | Cluster API | Cluster API |
| 3 | capi-provision | [Provisioning with Cluster API](/t/cluster-provisioning-with-capi/23366) |
| 3 | capi-manage | [Managing CAPI clusters](/t/microk8s-capi-management-cluster-ops/23370) |
| 3 | capi-upgrade | [Upgrading a CAPI cluster](/t/microk8s-capi-cluster-upgrades/23374) |
| 2 | images | Images and registries |
| 3 | registry-images | [Use local images](/t/registry-images/11272) |
| 3 | registry-public | [Use a public registry](/t/working-with-a-public-registry/11271) |
| 3 | registry-built-in | [Use the built-in registry](/t/built-in-registry/11274) |
| 3 | registry-private | [Use a private registry](/t/working-with-a-private-registry/11273) |
| 3 | sideload | [Side-load images](/t/image-side-loading/20437) |
| 2 | upgrading | [Upgrading](/t/upgrading-microk8s/15440) |
| 3 | manage-upgrades-with-a-snap-store-proxy | [Manage upgrades with a Snap Store Proxy](/t/how-to-control-microk8s-upgrades-using-a-snap-store-proxy/18221) |
| 3 | upgrade-cluster | [Upgrade a cluster](/t/upgrading-a-microk8s-cluster/18239) |
| 2 | security-how-to | [Security](.) |
| 3 | how-to-cis-harden | [CIS hardening](https://discuss.kubernetes.io/t/cis-hardening-and-assesment/24491)  |
| 3 | security-trivy | [Run security scans with Trivy](/t/how-to-run-a-security-scan-with-trivy/24812)
| 2 | how-to-storage | Storage |
| 3 | how-to-ceph | [Use MicroCeph/Ceph storage](/t/howto-setup-microk8s-with-micro-ceph-storage/25043) |
| 3 | how-to-nfs | [Use NFS for Persistent Volumes](/t/use-nfs-for-persistent-volumes/19035) |
| 2 | eks-d | [Installing EKS-D with MicroK8s](/t/install-eks-d-with-microk8s/21479) | 
| 2 | ingress | [Use the ingress addon](/t/addon-ingress/11259) |
| 2 | ports#heading--auth | [Use authentication and authorization](/t/services-and-ports/11263#heading--auth) |
| 2 | add-a-windows-worker-node-to-microk8s | [Add Windows workers](/t/add-a-windows-worker-node-to-microk8s/13782) |
| 2 | velero | [Backup workloads with Velero](/t/use-velero-for-backups/19034) |
| 2 | dockerhub-limits | [ Manage DockerHub rate limits](/t/dockerhub-download-rate-limits/18729) |
| 2 | build-microk8s-snap | [Build the MicroK8s snap](/t/building-the-microk8s-snap/24710) |
| 2 | troubleshooting | [Troubleshooting](/t/troubleshooting/11267) |
| 1 | explanation | [Explanation](/t/microk8s-explanation/19446)|
| 2 | high-availability | [High Availability](/t/high-availability-ha/11731) |
| 2 | snap-refreshes | [Snap package refreshes](/t/snap-refreshes/18791) |
| 2 | strict-confinement | [Strict confinement](/t/strict-confinement/21486) |
| 2 | explain-launch-config | [Launch configuration](/t/launch-configurations/23360) |
| 2 | clusterapi | [Cluster API (CAPI)](https://discuss.kubernetes.io/t/cluster-api-microk8s/23350) |
| 2 | explain-dual-stack | [Dual-stack (IPv4.IPv6)](/t/dual-stack-ipv4-ipv6/24786) |
| 2 | compliance | [ Security Compliance](/t/microk8s-compliance/24524) |
| 3 | cis-compliance | [CIS Hardening](https://discuss.kubernetes.io/t/cis-cluster-hardening/24492) |
| 1 | reference | [Reference](/t/microk8s-reference/19447)|
| 2 | release-notes | [Release notes](/t/release-notes/11266) |
| 2 | community | [Welcome to the community!](/t/microk8s-community/25687) |
| 2 | command-reference | [Command reference](/t/command-reference/11268) |
| 2 | services-and-ports | [Services and ports used](/t/services-and-ports/11263) |
| 2 | working-with-kubectl | [Working with kubectl](/t/working-with-kubectl/11258) |
| 2 | addons | [Addons](/t/microk8s-add-ons/11264) |
| 2 | ref-launch-config | [Launch configuration](/t/launch-configurations-reference/23358) |
| 2 | ref-build-env | [Build variables](https://discuss.kubernetes.io/t/build-time-variables/24705) |
| 2 | ref-ubuntu-pro | [Ubuntu Pro for MicroK8s](/t/ubuntu-pro-for-microk8s/25689) |
| 2 | get-in-touch | [Get in touch](/t/get-in-touch/11261) |
| 2 | troubleshooting#heading--report-bug | [Reporting a bug](/t/troubleshooting/11267#heading--report-bug) |
| 2 | docs/docs | [Contributing to docs](/t/creating-and-editing-microk8s-documentation/13681) |
|   | addons-test | [addon test page](/t/addons-table/22056) |
|   | addons#heading--list | [Available addons](/t/microk8s-add-ons/11264#heading--list) |
|   | addon-ambassador | [ambassador](/t/addon-ambassador/11806) |
|   | addon-cloudnative-pg | [cloudnative-pg](/t/addon-cloudnative-pg/26228) |
|   | addon-dashboard | [dashboard](/t/addon-dashboard/11289) |
|   | addon-dns | [dns](/t/addon-dns/11287) |
|   | addon-easyhaproxy | [EasyHAProxy](https://discuss.kubernetes.io/t/addon-easyhaproxy/23183) |
|   | addon-falco | [Falco](/t/addon-falco/26227) |
|   | addon-fluentd| [fluentd](/t/addon-fluentd/11262) |
|   | addon-gpu| [gpu](/t/add-on-gpu/11286) |
|   | addon-gopaddle-lite | [gopaddle-lite](/t/addon-gopaddle-lite/22120) |
|   | addon-host-access| [host-access](/t/addon-host-access/11260) |
|   | addon-inaccel| [inaccel](/t/addon-inaccel/17522) |
|   | addon-ingress| [ingress](/t/addon-ingress/11259) |
|   | addon-kata | [kata](/t/addon-kata/17010) |
|   | addon-keda | [keda](/t/addon-keda/14011) |
|   | addon-kubeflow | [kubeflow](/t/addon-kubeflow/12089) |
|   | addon-kube-ovn | [KubeOVN](/t/addon-kubeovn/20700) |
|   | addon-kubearmor | [KubeArmor](/t/addon-kubearmor/24862) |
|   | addon-kwasm | [KWasm](/t/addon-kwasm/22054) |
|   | addon-linkerd | [linkerd](/t/addon-linkerd/11285) |
|   | addon-metallb | [metallb](/t/addon-metallb/11790) |
|   | addon-microrocks | [microrocks](/t/addon-microcks/24904)
|   | addon-minio | [minio](/t/addon-minio/22132) |
|   | addon-multus | [multus](/t/addon-multus/11664) |
|   | addon-ngrok | [ngrok](/t/addon-ngrok/26411) |
|   | addon-ondat | [ondat](/t/addon-ondat/22161) |
|   | addon-openfaas | [openfaas](/t/addon-openfaas/14981) |
|   | addon-openebs | [openebs](/t/addon-openebs/15229) |
|   | addon-portainer | [portainer](/t/addon-portainer/13714) |
|   | addon-traefik | [traefik](/t/addon-traefik-ingress/13742) |
|   | addon-mayastor-1 | [mayastor](/t/addon-openebs-mayastor-clustered-storage/19451) |
|   | addon-mayastor | [mayastor](/t/addon-openebs-mayastor-clustered-storage-v2-0-0/23493) |
|  | addon-nfs | [nfs](/t/addon-nfs-server-provisioner/20903) |
|  | addon-ngrok | [ngrok](/t/addon-ngrok/26411) |
|  | addon-osm | [osm-edge](/t/addon-osm-edge/20969) |
|  | addon-parking | [addon-parking](/t/addon-parking/23186) |
|  | addon-hostpath-storage | [addon-hostpath](/t/addon-hostpath-storage/20878) |
|  | addon-cert-manager | [addon-cert-manager](/t/addon-cert-manager/21122) |
|  | addon-rook-ceph | [addon-rook-ceph](/t/addon-rook-ceph/25042) |
|  | addon-sosivio | [addon-sosivio](/t/addon-sosivio/21458) |
|  | addon-shifu | [addon-shifu](/t/addon-shifu/23327) |
|  | aws-user-guide |[AWS Appliance Deployment Guide](/t/microk8s-aws-appliance-deployment-guide/21664) |
|  | how-to | [How to Guides](/t/microk8s-how-to-guides/19442) |
|  | capi-management | [CAPI upgrades and migration](/t/microk8s-capi-management-cluster-ops/23370) | 

</details>

<h2>Redirects</h2>

[details=Mapping table]
| Path | Location |
| -- | -- |
| /docs/lxd | /docs/install-lxd |
[/details]

[How to manage Addons]: /t/how-to-manage-addons/23799
[Addons]: /t/microk8s-addons/11264
|  | addon-osm | [osm-edge](/t/addon-osm-edge/20969) |
|  | addon-parking | [addon-parking](/t/addon-parking/23186) |
|  | addon-hostpath-storage | [addon-hostpath](/t/addon-hostpath-storage/20878) |
|  | addon-cert-manager | [addon-cert-manager](/t/addon-cert-manager/21122) |
|  | addon-rook-ceph | [addon-rook-ceph](/t/addon-rook-ceph/25042) |
|  | addon-sosivio | [addon-sosivio](/t/addon-sosivio/21458) |
|  | addon-shifu | [addon-shifu](/t/addon-shifu/23327) |
|  | aws-user-guide |[AWS Appliance Deployment Guide](/t/microk8s-aws-appliance-deployment-guide/21664) |
|  | how-to | [How to Guides](/t/microk8s-how-to-guides/19442) |
|  | capi-management | [CAPI upgrades and migration](/t/microk8s-capi-management-cluster-ops/23370) | 
[/details] 

<h2>Redirects</h2>

[details=Mapping table]
| Path | Location |
| -- | -- |
| /docs/lxd | /docs/install-lxd |
[/details]

[How to manage Addons]: /t/how-to-manage-addons/23799
[Addons]: /t/microk8s-addons/11264